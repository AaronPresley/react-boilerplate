import * as React from 'react';

const ExampleComponent:React.SFC<{}> = () => (
    <p>Hello, World!!</p>
);

export default ExampleComponent;