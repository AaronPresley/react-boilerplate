import * as React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import { ExampleComponent } from './components';
import initializeStore from './state/store';

const store = initializeStore();

const App = () => (
  <Provider store={store}>
    <Router>
      <Switch>
        <Route component={ExampleComponent} />
      </Switch>
    </Router>
  </Provider>
);

render(<App />, document.getElementById('root'));
