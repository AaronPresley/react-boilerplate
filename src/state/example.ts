import { ActionObject } from './types';

// const baseName = 'react-boilerplate/example';
// export const SOME_EXAMPLE = `${baseName}/SET_PERMISSIONS_LOADING`;

export const initialState = {
};

export default (state = initialState, action:ActionObject) => {
  switch (action.type) {
    default:
      return {
        ...state,
      };
  }
};

// export const exampleFunction = () => (dispatch) => {
// };
