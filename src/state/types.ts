export interface ActionObject {
  type: string,
  payload: any,
};