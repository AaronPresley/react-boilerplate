import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import rootReducer from '.';

export default (initialState:object = {}) => {
  const enhancers = [applyMiddleware(thunk)];
  
  if (process.env.NODE_ENV !== 'production') {
    enhancers.push(composeWithDevTools());
  }

  return createStore(
    rootReducer,
    initialState,
    compose(...enhancers),
  );
}