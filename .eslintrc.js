module.exports = {
  'extends': 'airbnb',
  'parser': 'babel-eslint',
  'plugins': ['react', 'jsx', 'jsx-a11y', 'import'],
  'env': {
    'browser': true,
    'es6': true
  },
  'rules': {
    // Require spaces after a block's curly braces
    'block-spacing': [2, 'always'],

    // Requiring braces to be on the same line
    'brace-style': [2, 'stroustrup'],

    // Enforce all curly brackets to have a space after
    'curly': [2, 'all'],

    'no-trailing-spaces': [2],

    // Ensure no lines are more than 120 lines
    'max-len': [2, { 'code': 120 }],

    // Don't allow pbject props to have quotes unless they have to
    'quote-props': [2, 'as-needed'],

    // Require the use of single quotes
    'quotes': [2, 'single', {
      'avoidEscape': true,
    }],

    'import/no-named-as-default': [0],

    // Requires closing JSX tags to match open tag spacing
    'react/jsx-closing-bracket-location': [2],

    // Don't require JSX tags to be on their own line
    'react/jsx-closing-tag-location': [0],
    
    // Don't allow spaces between curly praces within a prop
    'react/jsx-curly-spacing': [2, {'when': 'never'}],
    
    // Don't allow spaces around the = in props
    'react/jsx-equals-spacing': [2, 'never'],
    
    // Ensure our react files are just js files
    'react/jsx-filename-extension': [2, {
      'extensions': ['.js']
    }],
    
    // Require all props to have 2 spaces when props break lines
    'react/jsx-indent-props': [2, 2],

    // Require there to be a spacing before closing bracket
    'react/jsx-tag-spacing': [2, {
      'beforeSelfClosing': 'always',
      'beforeClosing': 'never',
    }],
    
    // It's okay to put HTML within a component
    'react/no-unescaped-entities': [0],

    // Enforcing an outdated rule, per:
    //https://github.com/airbnb/javascript/issues/684#issuecomment-381674576
    'react/no-did-mount-set-state': [0],

    'react/self-closing-comp': [0, {
      'component': true,
      'html': false,
    }],

    // Require links to have links, including the React Router <Link> component
    'jsx-a11y/anchor-is-valid': [2, {
      'components': ['Link'],
      'specialLink': ['to'],
    }],

    // Form fields don't have to have for / id / etc
    'jsx-a11y/label-has-for': [0],
  },

  'globals': {
    describe: true,
    expect: true,
    it: true,
  }
};
