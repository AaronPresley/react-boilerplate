
import * as emotion from 'emotion';
import { createSerializer } from 'jest-emotion';
import Adapter from 'enzyme-adapter-react-16';
import { configure } from 'enzyme';

// Telling emotion to output the full CSS and not the randomly
// generated className
expect.addSnapshotSerializer(createSerializer(emotion));

// Configuring our enzyme adapter so we don't have to do it
// in each file
configure({ adapter: new Adapter() });
