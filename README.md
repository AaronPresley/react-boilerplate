# React Boilerplate

This is my preferred starting point with any new React project. I keep it pretty updated with my favorite workflow, as well as a few example components for reference.

## Installation

Clone the repo into your local machine. Then run `npm install`.

## Local Development

Now start the project with:

```
yarn start
```

You should now be able to visit http://localhost:8080 to see the app running
with the example components. Each change to the JS will automatically be
recompiled.

## Running Tests

To run **unit tests only** (without linting), run the following:

```
yarn test:run
```

To run a full test, including linting, run:

```
yarn test
```

To run tests with coverage:

```
yarn test:coverage
```

## Linting

You can run lint tests with:

```
yarn lint
```

## Compiling for Production

To compile your project for use on a production site, run the following:

```
yarn build:prod
```

This will compile and minify your project, then place the files into the `/dist`
folder within your project's root directory.
