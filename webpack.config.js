const path = require('path');
const webpack = require('webpack');

const NODE_ENV = process.env.NODE_ENV === 'production' ? 'production' : 'development';
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  mode: NODE_ENV,

  entry: [
    './src/index.tsx',
  ],

  devtool: 'inline-source-map',

  output: {
    path: path.join(__dirname, './dist/'),
    filename: 'bundle.js',
  },

  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.json'],
  },

  module: {
    rules: [
      {
        test: /\.(js|ts)x?$/,
        exclude: /node_modules/,
        use: [
          { loader: 'babel-loader' },
        ],
      },
      {
        test: /\.(png|woff|woff2|eot|ttf|svg)$/,
        use: [
          { loader: 'url-loader' },
        ],
      },
    ],
  },

  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV),
      },
    }),
    new CopyWebpackPlugin([
      {
        from: 'src/index.html',
      },
      {
        context: path.resolve('src'),
        from: 'assets/**/*.*',
      },
    ]),
  ],
};
