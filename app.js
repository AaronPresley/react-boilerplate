/* eslint-disable global-require, no-console */

const compression = require('compression');
const express = require('express');
const path = require('path');

const startApp = (targetPort) => {
  const app = express();
  const staticPath = path.join(__dirname, 'dist');

  if (process.env.NODE_ENV === 'development') {
    console.log('\n\n\n--- 🚨 DEV MODE 🚨 ---');
    const webpack = require('webpack');
    const middleware = require('webpack-dev-middleware');
    const compiler = webpack(require('./webpack.config'));

    app.use(middleware(compiler, {
      writeToDisk: true,
    }));
  }

  else {
    console.log('\n\n\n--- 🚨 PROD MODE 🚨 ---');
    app.use(compression());
  }

  // Hitting a special URL for our static files
  app.use('/static/', express.static(staticPath));
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(staticPath, 'index.html'));
  });

  app.listen(targetPort, () => console.log(`http://localhost:${targetPort}`));
};

startApp(8080);
